# Xplore
### BY AARON PARKER, DAVID GARCIA, ELVIS LEE

## Introduction

Xplore is a submission based social media platform that enables travellers across the globe to submit posts showcasing cuisine, architecture, landscape and much more.

## Major Functions Provided

> ### View Posts
> * **User**        - view posts of a specified user
> * **All Posts**   - view all posts 

> ### Users
> * **New**         - create a new user
> * **Existing**    - login as existing user

## To-Do

Some features we would like to implement into this webapp include:
###
> * Add a homepage if you enter an empty path which would hold our teams logo
> * Implement transactions, which our ORM supports!
> * Forward user based on successful login

## License

The project was a demo to apply Java Reflection and Annotations to create a custom library. Our team created the AdeOrm within two weeks, there is a lot of room for improvement.
Feel free to use it and make contribution.
