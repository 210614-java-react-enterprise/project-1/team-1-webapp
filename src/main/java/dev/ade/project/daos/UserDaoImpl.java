package dev.ade.project.daos;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.orm.FieldPair;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class UserDaoImpl implements UserDao {
    private static UserDaoImpl userDao;
    private final AdeOrm uAdeOrm;
    private String url = "jdbc:postgresql://training-db.czu9b8kfiorj.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project-1";
    final String USERNAME = System.getenv("USERNAME");
    final String PASSWORD = System.getenv("PASSWORD");

    private UserDaoImpl() {
        uAdeOrm = new AdeOrm(User.class);
        uAdeOrm.setConnection(url);
    }

    public UserDaoImpl(AdeOrm uAdeOrm) {
        this.uAdeOrm = uAdeOrm;
        uAdeOrm.setConnection(url);
    }

    /**
     * The getUserDaoInstance method to ensure a singleton
     * userDao is returned to the methode invoked it.
     */
    public static UserDaoImpl getUserDaoInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl();
        }
        return userDao;
    }

    @Override
    public User getUser(String uniCol, String colValue) {
        if (uniCol == null || colValue == null) {
            return null;
        }
        try {
            return (User)uAdeOrm.get(uniCol, colValue);
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getColumns(String uniCol, String colValue, String... columns) {
        if (uniCol == null || colValue == null || columns == null) {
            return null;
        }
        try {
            return uAdeOrm.getColumns(uniCol, colValue, columns).stream()
                    .map(e -> (String) e).collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<List<String>> getRecordsInOrder(List<String> columnNames, String colName, Object colValue,
                                                String orderCol, String order) {
        if (columnNames == null || colName == null || colValue == null || orderCol == null || order == null) {
            return null;
        }
        try {
            return uAdeOrm.getRecordsInOrder(columnNames, colName, colValue, orderCol, order).stream()
                    .map(e -> e.stream()
                            .map(s -> (String) s).collect(Collectors.toList()))
                    .collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getAll() {
        try {
            return uAdeOrm.getAll().stream().map(e -> (User)e).collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getAllInOrder(String orderCol, String order) {
        if (orderCol == null || order == null) {
            return null;
        }
        try {
            return uAdeOrm.getAllInOrder(orderCol, order).stream().map(e -> (User)e).collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getWithCriteria(List<FieldPair> fieldPairs, String criteria) {
        if (fieldPairs == null || criteria == null) {
            return null;
        }
        try {
            return uAdeOrm.getWithCriterion(fieldPairs, criteria).stream().map(e -> (User) e).collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<List<String>> getJoint(String jType, String pkA, String tableB, String fkA, List<String> columnNames) {
        if (jType == null || pkA == null || tableB == null || fkA == null || columnNames == null) {
            return null;
        }
        try {
            return uAdeOrm.getJoint(jType, pkA, tableB, fkA, columnNames).stream()
                    .map(e -> e.stream()
                            .map(s -> (String) s).collect(Collectors.toList()))
                    .collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<List<String>> getJointWhere(String jType, String pkA, String tableB, String fkA,
                                            List<String> columnNames, String fieldName, Object fieldValue) {
        if (jType == null || pkA == null || tableB == null || fkA == null || columnNames == null ||
                fieldName == null || fieldValue == null) {
            return null;
        }
        try {
            return uAdeOrm.getJointWhere(jType, pkA, tableB, fkA, columnNames, fieldName, fieldValue).stream()
                    .map(e -> e.stream()
                            .map(s -> (String) s).collect(Collectors.toList()))
                    .collect(Collectors.toList());
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean addUser(User user) {
        if (user == null) {
            return false;
        } else {
            try {
                return uAdeOrm.add(user);
            } catch (ArgumentFormatException | SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean deleteUser(Object pkValue) {
        if (pkValue == null) {
            return false;
        }
        try {
            return uAdeOrm.delete("username", pkValue);
        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateUser(User user) {
        if (user == null) {
            return false;
        }
        try {
            return uAdeOrm.update(user);
        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
