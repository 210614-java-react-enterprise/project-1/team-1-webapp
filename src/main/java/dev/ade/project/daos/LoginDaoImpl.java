package dev.ade.project.daos;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LoginDaoImpl implements LoginDao {

    private static LoginDaoImpl loginDao;
    private final AdeOrm uAdeOrm;
    private String url = "jdbc:postgresql://training-db.czu9b8kfiorj.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project-1";
    final String USERNAME = System.getenv("USERNAME");
    final String PASSWORD = System.getenv("PASSWORD");


    private LoginDaoImpl(){
        uAdeOrm = new AdeOrm(User.class);
        uAdeOrm.setConnection(url);
    }

    // constructor with Orm parameter so we can inject mock orm if we need to
    public LoginDaoImpl(AdeOrm adeOrm){
        this.uAdeOrm = adeOrm;
        uAdeOrm.setConnection(url);
    }

    public static LoginDaoImpl getLoginDaoInstance(){
        if(loginDao == null){
            loginDao = new LoginDaoImpl();
        }
        return loginDao;
    }


    @Override
    public boolean userExistsCheck(String username) throws ArgumentFormatException {
        User user1 = (User) uAdeOrm.get("username",username);

        return username.equals(user1.getUsername());
    }

    @Override
    public boolean passwordCheck(String username, String pass) throws ArgumentFormatException {
        // validate whether input password is correct with the username in database
        List<String> colName = Arrays.asList("username","user_password");
        List<List<Object>> list = uAdeOrm.getRecordsInOrder(colName,"username",username,"username","asc");
        String dbPass = (String) list.get(0).get(1);

        return pass.equals(dbPass);
    }

    @Override
    public User instantiateUser(String username) throws ArgumentFormatException {
        return (User) uAdeOrm.get("username", username);
    }
}
