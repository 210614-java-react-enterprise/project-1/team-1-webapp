package dev.ade.project.daos;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;

public interface LoginDao {

    boolean userExistsCheck(String user) throws ArgumentFormatException, ArgumentFormatException;

    boolean passwordCheck(String user, String pass) throws ArgumentFormatException;

    User instantiateUser(String username) throws ArgumentFormatException, ArgumentFormatException;
}
