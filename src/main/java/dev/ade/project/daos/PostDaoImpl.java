package dev.ade.project.daos;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PostDaoImpl implements PostDao{

    Post post = new Post();
    AdeOrm orm = new AdeOrm(post.getClass());

    public PostDaoImpl(AdeOrm orm) {
        this.orm = orm;
    }

    @Override
    public List<Post> getAllPosts() {
        try {
            List<Post> postList = orm.getAll().stream().map(p->(Post)p).collect(Collectors.toList());
            return postList;
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Post> getAllPosts(User user) {
        try {
            List<Post> allPostsList = orm.getAll().stream().map(p->(Post)p).collect(Collectors.toList());
            List<Post> userPostsList = new ArrayList<>();
            for (Post p : allPostsList) {
                if (p.getUsername().matches(user.getUsername())) {
                    userPostsList.add(p);
                }
            }
            return userPostsList;
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean addNewPost(Post newPost) {
        try {
           return orm.add(newPost);
        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deletePost(Post post) {
        try {
            if (orm.delete(post)) return true;
            else return false;
        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean editPost(Post post) {
        try {
            if (orm.update(post)) return true;
            else return false;
        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


}
