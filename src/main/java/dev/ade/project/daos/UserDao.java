package dev.ade.project.daos;

import dev.ade.project.models.User;
import dev.ade.project.orm.FieldPair;

import java.util.List;

public interface UserDao {
    User getUser(String pk, String pkValue);
    List<String> getColumns(java.lang.String uniCol, java.lang.String colValue, java.lang.String... columns);
    List<List<String>> getRecordsInOrder(List<String> columnNames, String colName, Object colValue,
                                         String orderCol, String order);
    List<User> getAll();
    List<User> getAllInOrder(String orderCol, String order);
    List<User> getWithCriteria(List<FieldPair> fieldPairs, String criteria);
    List<List<String>> getJoint(String jType, String pkA, String tableB, String fkA, List<String> columnNames);
    List<List<String>> getJointWhere(String jType, String pkA, String tableB, String fkA,
                                     List<String> columnNames, String fieldName, Object fieldValue);
    boolean addUser(User user);
    boolean deleteUser(Object pkValue);
    boolean updateUser(User user);
}
