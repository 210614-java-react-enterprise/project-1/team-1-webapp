package dev.ade.project.daos;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import dev.ade.project.models.User;

import java.util.List;

public interface PostDao {

    public List<Post> getAllPosts();
    public List<Post> getAllPosts(User user);
    public boolean addNewPost(Post newPost);
    public boolean deletePost(Post post) throws ArgumentFormatException;
    public boolean editPost(Post post);
}

