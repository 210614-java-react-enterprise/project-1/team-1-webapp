package dev.ade.project.util;

public class JsonUtil {

    public static String xmlRegToJson(String input) {
        StringBuilder sb = new StringBuilder();
        String[] pairs = input.replaceAll("=", ":").split("&");
        for (String s : pairs) {
            int i = s.indexOf(":");
            sb.append("\"").append(s.substring(0,i)).append("\"").append(":").append("\"")
                    .append(s.substring(i+1)).append("\"").append(",");
        }
        String str = sb.toString();
        return "{" + str.substring(0, str.length()-1) + "}";
    }

}
