package dev.ade.project.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.ade.project.daos.PostDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.services.PostService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class PostServlet extends HttpServlet {

    private final String url = "jdbc:postgresql://training-db.czu9b8kfiorj.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project-1";

    public PostServlet () {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp); // 405 response

        List<Post> posts;
        // session
        HttpSession s = req.getSession(false);
        if (s!=null) {
            User user = (User) s.getAttribute("user");
            Post p = new Post();
            final AdeOrm orm = new AdeOrm(p.getClass());
            orm.setConnection(url);

            PostService postService = new PostService(new PostDaoImpl(orm));

            posts = postService.getAll(user);
        } else {
            Post p = new Post();
            final AdeOrm orm = new AdeOrm(p.getClass());
            orm.setConnection(url);

            PostService postService = new PostService(new PostDaoImpl(orm));
            posts = postService.getAll();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(posts);
        try(PrintWriter pw = resp.getWriter()) {
            pw.write(json);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doPost(req, resp); // 405 response
        BufferedReader reader = req.getReader();
        Post p = new Post();
        final AdeOrm orm = new AdeOrm(p.getClass());
        orm.setConnection(url);
        PostService postService = new PostService(new PostDaoImpl(orm));
        // obtain json from request body
        String postJson = reader.readLine();

        // convert json to post object
        ObjectMapper objectMapper = new ObjectMapper();
        Post post = objectMapper.readValue(postJson, Post.class);

        System.out.println(post);
        // use service to add new post
        if (postService.addNewPost(post)) {
            resp.setStatus(200);
        } else {
            resp.sendError(400);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doDelete(req, resp); // 405 response
        BufferedReader reader = req.getReader();
        PrintWriter pw = resp.getWriter();
        Post p = new Post();
        final AdeOrm orm = new AdeOrm(p.getClass());
        orm.setConnection(url);
        PostService postService = new PostService(new PostDaoImpl(orm));

        String postToUpdateJson = reader.readLine();

        // convert json to post object
        ObjectMapper objectMapper = new ObjectMapper();
        Post post = objectMapper.readValue(postToUpdateJson, Post.class);

        System.out.println(post);
        if (postService.editPost(post)) {
            resp.setStatus(200);
            pw.write("<h2>Post was updated</h2>");
        } else {
            pw.write("<h2>Sorry, that post does not exist.</h2>");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doPut(req, resp); // 405 response
        BufferedReader reader = req.getReader();
        PrintWriter pw = resp.getWriter();
        Post p = new Post();
        final AdeOrm orm = new AdeOrm(p.getClass());
        orm.setConnection(url);
        PostService postService = new PostService(new PostDaoImpl(orm));
        // obtain json from request body
        String postJson = reader.readLine();

        // convert json to post object
        ObjectMapper objectMapper = new ObjectMapper();
        Post post = objectMapper.readValue(postJson, Post.class);

        // send some indicator of whether or not something was deleted successfully

        try {
            if (postService.deletePost(post)) {
                resp.setStatus(200);
                pw.write("<h2>Post was deleted</h2>");
            } else {
                pw.write("<h2>Sorry, that post does not exist.</h2>");
            }
        } catch (ArgumentFormatException e) {
            e.printStackTrace();
        }
    }
}
