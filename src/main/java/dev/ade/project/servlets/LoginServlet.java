package dev.ade.project.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ade.project.daos.LoginDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.services.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


public class LoginServlet extends HttpServlet {

    private final LoginService loginService = new LoginService(LoginDaoImpl.getLoginDaoInstance());
    public LoginServlet(){ }


    /**
     *
     * @param req takes in request with Json in body to check if user exists in database
     * @param resp creates a session on the server with the logged in user's parameters
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

        try(BufferedReader reader = req.getReader();
            PrintWriter pw = resp.getWriter()){

            String usernamePassJson = reader.readLine();

            ObjectMapper objectMapper = new ObjectMapper();

            User loggedInUser = objectMapper.readValue(usernamePassJson, User.class); // pass in username and password into constructor

            try{
                if(loggedInUser.getUsername() == null || loggedInUser.getUserPassword() == null){
                    resp.sendError(400,"You did not pass in a username or password in the request body!");
                    return;
                }
            }catch(NullPointerException e){
                resp.sendError(400,"You did not pass in a username or password in the request body!");
                return;
            }

            if(!loginService.userCheck(loggedInUser.getUsername()))
                { resp.sendError(404, "username not found"); return; }

            boolean passCheck = loginService.passCheck(loggedInUser.getUsername(), loggedInUser.getUserPassword());

            if(!passCheck){ resp.sendError(401, "password is incorrect"); return;}
            loggedInUser = loginService.instantiateUser(loggedInUser.getUsername());

            resp.setHeader("Content-Type","text/html;charset=utf-8");
            pw.write("<h2> Welcome " + loggedInUser.getUsername() + " </h2>");

            HttpSession s = req.getSession();
            s.setAttribute("LoggedIn", true);
            s.setAttribute("user", loggedInUser);

            Cookie cookie = new Cookie("loggedIn","true");
            cookie.setMaxAge(3600);
            resp.addCookie(cookie);

        } catch (ArgumentFormatException | SQLException e) {
            e.printStackTrace();
        }
    }
}
