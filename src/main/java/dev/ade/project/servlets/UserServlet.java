package dev.ade.project.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ade.project.daos.UserDaoImpl;
import dev.ade.project.models.User;
import dev.ade.project.orm.FieldPair;
import dev.ade.project.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UserServlet extends HttpServlet {

    private final UserService userService = new UserService(UserDaoImpl.getUserDaoInstance());

    public UserServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users;
        User user;
        String username = req.getParameter("username");
        String orderCol = req.getParameter("order-column");
        String order = req.getParameter("order");

        String firstName = req.getParameter("first-name");
        String lastName = req.getParameter("last-name");
        String gender = req.getParameter("gender");
        String password = req.getParameter("password");

        FieldPair fpFirstName;
        FieldPair fpLastName;
        FieldPair fpGender;
        FieldPair fpPassword;
        List<FieldPair> fieldPairs = new ArrayList<>();
        if (firstName != null){
            fpFirstName = new FieldPair("first_name", firstName);
            fieldPairs.add(fpFirstName);
        }
        if (lastName != null){
            fpLastName = new FieldPair("last_name", lastName);
            fieldPairs.add(fpLastName);
        }
        if (gender != null){
            fpGender = new FieldPair("gender", gender);
            fieldPairs.add(fpGender);
        }
        if (password != null){
            fpPassword = new FieldPair("user_password", password);
            fieldPairs.add(fpPassword);
        }

        // get all users by default
        if (username == null && orderCol == null && order == null && firstName == null &&
                lastName == null && gender == null && password == null) {
            users = userService.getAll();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(users);
            try (PrintWriter pw = resp.getWriter()) {
                pw.write(json);
            }
        }

        // get user by primary key
        if (username != null) {
            user = userService.getUser(username);
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(user);
            try (PrintWriter pw = resp.getWriter()) {
                pw.write(json);
            }
        }

        // get all records in order
        if (orderCol != null && order != null) {
            users = userService.getAllInOrder(orderCol, order);
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(users);

            try (PrintWriter pw = resp.getWriter()) {
                pw.write(json);
            }
        }

        if (fieldPairs.size() == 1) {
            users = userService.getWithCriteria(fieldPairs, "no");
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(users);
            try (PrintWriter pw = resp.getWriter()) {
                pw.write(String.valueOf(users));
            }
        } else {
            users = userService.getWithCriteria(fieldPairs, "and");
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(users);
            try (PrintWriter pw = resp.getWriter()) {
                pw.write(String.valueOf(users));
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try(BufferedReader reader = req.getReader();
            PrintWriter pw = resp.getWriter()){

            String json = reader.readLine();

            // convert json to user object
            ObjectMapper objectMapper = new ObjectMapper();
            User user = objectMapper.readValue(json, User.class);

            // use service to add new user
            if (userService.addUser(user)) {
                User newlyCreatedUser = userService.getUser(user.getUsername());
                resp.setStatus(201);
                pw.write(objectMapper.writeValueAsString(newlyCreatedUser));
            } else {
                resp.sendError(400);
            }
        }
    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try(BufferedReader reader = req.getReader();
            PrintWriter pw = resp.getWriter()){

            String json = reader.readLine();

            // convert json to user object
            ObjectMapper objectMapper = new ObjectMapper();
            User user = objectMapper.readValue(json, User.class);

            // use service to update user record
            if (userService.updateUser(user)) {
                User newlyUpdatedUser = userService.getUser(user.getUsername());
                resp.setStatus(200);
                pw.write(objectMapper.writeValueAsString(newlyUpdatedUser));
            } else {
                resp.sendError(400);
            }
        }
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try(BufferedReader reader = req.getReader();
            PrintWriter pw = resp.getWriter()){

            String json = reader.readLine();

            // convert json to user object
            ObjectMapper objectMapper = new ObjectMapper();
            User user = objectMapper.readValue(json, User.class);

            // use service to delete the user
            if (userService.deleteUser(user.getUsername())) {
                resp.setStatus(200);
            } else {
                resp.sendError(400);
            }
        }
    }

}
