package dev.ade.project.services;


import dev.ade.project.daos.LoginDao;
import dev.ade.project.daos.LoginDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;

import java.sql.SQLException;

public class LoginService {

    private final LoginDaoImpl loginDao;

    public LoginService(LoginDaoImpl loginDao){
        this.loginDao = loginDao;
    }

    public boolean userCheck(String user) throws ArgumentFormatException {
        return loginDao.userExistsCheck(user);
    }

    public boolean passCheck(String user, String pass) throws ArgumentFormatException {
        return loginDao.passwordCheck(user, pass);
    }

    public User instantiateUser(String username) throws ArgumentFormatException, SQLException {
        return loginDao.instantiateUser(username);
    }

}
