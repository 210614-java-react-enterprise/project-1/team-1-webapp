package dev.ade.project.services;

import dev.ade.project.daos.PostDao;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import dev.ade.project.models.User;

import java.util.List;

public class PostService {

    private PostDao postDao;

    public PostService(PostDao postDao) {
        this.postDao = postDao;
    }

    public List<Post> getAll() {
        return postDao.getAllPosts();
    }
    public List<Post> getAll(User user) {
        return postDao.getAllPosts(user);
    }

    public boolean addNewPost(Post post) {
        return postDao.addNewPost(post);
    }

    public boolean deletePost(Post post) throws ArgumentFormatException {
        return postDao.deletePost(post);
    }

    public boolean editPost(Post post) {
        return postDao.editPost(post);
    }
}

