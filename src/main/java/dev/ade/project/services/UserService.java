package dev.ade.project.services;

import dev.ade.project.daos.UserDao;
import dev.ade.project.models.User;
import dev.ade.project.orm.FieldPair;

import java.util.List;

public class UserService {

    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public User getUser(String pkValue) {
        return userDao.getUser("username", pkValue);
    }



    public List<String> getColumns(String pkValue, String... columns) {
        return userDao.getColumns("username", pkValue, columns);
    }

    public List<List<String>> getRecordsInOrder(List<String> columnNames, String colName, Object colValue,
                                                String orderCol, String order) {
        return userDao.getRecordsInOrder(columnNames, colName, colValue, orderCol, order);
    }

    public List<User> getAll() {
        return userDao.getAll();
    }

    public List<User> getAllInOrder(String orderCol, String order) {
        return userDao.getAllInOrder(orderCol, order);
    }

    public List<User> getWithCriteria(List<FieldPair> fieldPairs, String criteria) {
        return userDao.getWithCriteria(fieldPairs, criteria);
    }

    public List<List<String>> getJoint(String jType, String pkA, String tableB, String fkA, List<String> columnNames) {
        return userDao.getJoint(jType, pkA, tableB, fkA, columnNames);
    }

    public List<List<String>> getJointWhere(String jType, String pkA, String tableB, String fkA,
                                            List<String> columnNames, String fieldName, Object fieldValue) {
        return userDao.getJointWhere(jType, pkA, tableB, fkA, columnNames, fieldName, fieldValue);
    }

    public boolean addUser(User user) {
        return userDao.addUser(user);
    }

    public boolean deleteUser(String pkValue) {
        return userDao.deleteUser(pkValue);
    }

    public boolean updateUser(User user) {
        return userDao.updateUser(user);
    }


}
