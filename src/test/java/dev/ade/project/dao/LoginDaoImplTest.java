package dev.ade.project.dao;

import dev.ade.project.daos.LoginDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginDaoImplTest {

    @InjectMocks
    @Spy
    private LoginDaoImpl loginDao;

    @Mock
    AdeOrm mockOrm;

    @BeforeAll
    public void setupMocks() throws SQLException, FileNotFoundException, ArgumentFormatException {
        initMocks(this);

        loginDao = new LoginDaoImpl(mockOrm);
    }

    @Test
    public void userExistsCheckTest() throws ArgumentFormatException, SQLException {

        User user = new User("The","Tester",'M',"thetester","password");
        User spyUser = spy(user);

        when(mockOrm.get(any(String.class),any(Object.class))).thenReturn(spyUser);

        assertTrue(loginDao.userExistsCheck("thetester"));
    }

    @Test
    public void passwordCheckTest() throws ArgumentFormatException, SQLException {
        String username = "alpha";
        String pass = "password123";
        List<List<Object>> list = Arrays.asList(Arrays.asList(username,pass));

        when(mockOrm.getRecordsInOrder(any(List.class),any(String.class),any(Object.class),any(String.class),any(String.class)))
                .thenReturn(list);

        assertTrue(loginDao.passwordCheck(username,pass));
    }

    @Test
    public void instantiateUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("The","Tester",'M',"thetester","password");
        User spyUser = spy(user);

        assertEquals(spyUser.toString(), loginDao.instantiateUser("thetester").toString());
    }

}
