package dev.ade.project.dao;

import dev.ade.project.daos.UserDao;
import dev.ade.project.daos.UserDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserDaoImplTest {

    @InjectMocks
    @Spy
    private UserDaoImpl userDao;

    @Mock
    AdeOrm mockOrm;

    @BeforeAll
    public void setupMocks() throws ArgumentFormatException, SQLException {
        initMocks(this);
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User user2 = new User("Richelle", "Hunt", 'F', "beta", "123");
        User user3 = new User("Jorge", "Olivero", 'M', "Charlie", "123");
        List<User> spyUsers = Arrays.asList(user, user2, user3);

        doReturn(spyUsers).when(mockOrm).getAll();
        doReturn(true).when(mockOrm).add(any(User.class));
        doReturn(true).when(mockOrm).update(any(User.class));
        doReturn(true).when(mockOrm).delete(any(String.class), any(Object.class));
        doReturn(user).when(mockOrm).get(any(String.class), any(String.class));

        userDao = new UserDaoImpl(mockOrm);
    }

    @Test
    public void getUserTest() throws ArgumentFormatException, SQLException {
        assertEquals("alpha", userDao.getUser("username","alpha").getUsername());
    }

    @Test
    public void getAllTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User user2 = new User("Richelle", "Hunt", 'F', "beta", "123");
        User user3 = new User("Jorge", "Olivero", 'M', "Charlie", "123");
        assertEquals(3, userDao.getAll().size());
    }

    @Test
    public void addUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userDao.addUser(spyUser));
    }

    @Test
    public void deleteUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userDao.deleteUser(spyUser.getUsername()));
    }

    @Test
    public void updateUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userDao.updateUser(spyUser));
    }

}
