package dev.ade.project.dao;

import dev.ade.project.daos.PostDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PostDaoImplTest {

    @InjectMocks
    @Spy
    private PostDaoImpl postDao;

    @Mock
    AdeOrm mockOrm;

    @BeforeAll
    public void setupMocks() throws SQLException, FileNotFoundException, ArgumentFormatException {
        initMocks(this);
        postDao = new PostDaoImpl(mockOrm);
    }

    @Test
    public void testGetAllPosts() throws SQLException, ArgumentFormatException {
        Post post1 = new Post(201, "beta","Title1","Country1","City1","tag1", 5);
        Post post2 = new Post(202, "charlie","Title2","Country2","City2","tag2", 5);
        Post post3 = new Post(203, "alpha","Title3","Country3","City3","tag3", 5);
        List<Post> postList = Arrays.asList(post1, post2, post3);

        doReturn(postList).when(mockOrm).getAll();
        assertEquals(postList, postDao.getAllPosts());
    }

    @Test
    public void testAddNewPost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);

        doReturn(true).when(mockOrm).add(any(Object.class));
        assertTrue(postDao.addNewPost(spyPost));
    }

    @Test
    public void testDeletePost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);

        doReturn(true).when(mockOrm).delete(any(Object.class));
        assertTrue(postDao.deletePost(spyPost));
    }

    @Test
    public void testEditPost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);

        doReturn(true).when(mockOrm).update(any(Object.class));
        assertTrue(postDao.editPost(spyPost));
    }
}
