package dev.ade.project.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.models.User;
import dev.ade.project.services.LoginService;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.Buffer;
import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoginServletRemoteTest {

    @BeforeAll
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHttpSession() throws IOException, ServletException, ArgumentFormatException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        LoginService loginService = mock(LoginService.class);
        BufferedReader reader = mock(BufferedReader.class);
        ObjectMapper objectMapper = mock(ObjectMapper.class);
        AdeOrm adeOrm = mock(AdeOrm.class);
        User user = mock(User.class);
        when(reader.readLine()).thenReturn("String");
        when(loginService.userCheck("alpha")).thenReturn(true);
        when(loginService.passCheck("alpha","password123")).thenReturn(true);
        when(adeOrm.get("username","mockvalue")).thenReturn(user);
        when(user.getUsername()).thenReturn("test");

        new LoginServlet().doPost(request,response);

        /*HttpSession s = request.getSession();
        User user = (User) s.getAttribute("user");
        assertEquals("alpha",user.getUsername());*/

    }

    /*@BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("setup.sql"));
        }
    }

    @AfterAll
    public static void runTeardown() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("teardown.sql"));
        }

    }*/
}
