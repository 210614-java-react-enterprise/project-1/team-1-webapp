package dev.ade.project.servlets;

import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.pojo.Post;
import dev.ade.project.pojo.User;
import dev.ade.project.util.ConnectionUtil;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import javax.servlet.ServletException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PostServletTest {

    @InjectMocks
    @Spy
    private PostServlet postServlet;

    @Mock
    private AdeOrm adeOrm = new AdeOrm();

    @Mock
    User user = new User();
    Class<?> userClass = user.getClass();

    @Mock
    AdeOrm uAdeOrm = new AdeOrm(userClass);

    @Mock
    Post post = new Post();
    Class<?> postClass = post.getClass();

    @Mock
    AdeOrm pAdeOrm = new AdeOrm(postClass);

    @BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("setup.sql"));
        }
    }

    @Test
    public void testDoGet() throws ServletException, IOException, ArgumentFormatException {

    }

    @Test
    public void testDoPost() throws ServletException, IOException, ArgumentFormatException {

    }

    @Test
    public void testDoPut() throws ServletException, IOException, ArgumentFormatException {

    }

    @Test
    public void testDoDelete() throws ServletException, IOException, ArgumentFormatException {

    }

    @AfterAll
    public static void runTeardown() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("teardown.sql"));
        }

    }
}
