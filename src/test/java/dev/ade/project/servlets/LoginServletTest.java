package dev.ade.project.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.services.LoginService;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.Buffer;
import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginServletTest {

    @Spy
    @InjectMocks
    LoginServlet loginServlet;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Mock
    BufferedReader reader;

    @Mock
    LoginService loginService;

    @BeforeAll
    public void setup() throws IOException {
        initMocks(this);
    }

    @Test
    public void testBufferedReader() throws IOException, ServletException, ArgumentFormatException {
        doReturn(reader).when(request).getReader();
        doReturn("{\"username\":\"thetester\",\"userPassword\":\"password\"}").when(reader).readLine();
            loginServlet.doPost(request, response);

    }

    /*@BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("setup.sql"));
        }
    }

    @AfterAll
    public static void runTeardown() throws SQLException, FileNotFoundException {
        try (Connection connection = ConnectionUtil.getConnection()) {
            RunScript.execute(connection, new FileReader("teardown.sql"));
        }

    }*/
}
