package dev.ade.project.services;

import dev.ade.project.daos.PostDao;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PostServiceTest {
    @InjectMocks
    @Spy
    private PostService postService;

    @Mock
    PostDao postDao;

    @BeforeAll
    public void setupMocks() throws SQLException, FileNotFoundException, ArgumentFormatException {
        initMocks(this);

        postService = new PostService(postDao);

    }

    @Test
    public void testGetAll() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post post1 = new Post(201, "beta","Title1","Country1","City1","tag1", 5);
        Post post2 = new Post(202, "charlie","Title2","Country2","City2","tag2", 5);
        Post post3 = new Post(203, "alpha","Title3","Country3","City3","tag3", 5);
        List<Post> spyPostList = Arrays.asList(post, post1, post2, post3);
        doReturn(spyPostList).when(postDao).getAllPosts();
        assertEquals(spyPostList, postService.getAll());
    }

    @Test
    public void testAddNewPost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);
        doReturn(true).when(postDao).addNewPost(any(Post.class));
        assertTrue(postService.addNewPost(spyPost));
    }

    @Test
    public void testDeletePost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);
        doReturn(true).when(postDao).deletePost(any(Post.class));
        assertTrue(postService.deletePost(spyPost));
    }

    @Test
    public void testEditPost() throws SQLException, ArgumentFormatException {
        Post post = new Post(200, "alpha","Title","Country","City","tag", 5);
        Post spyPost = spy(post);
        doReturn(true).when(postDao).editPost(any(Post.class));
        assertTrue(postService.editPost(spyPost));
    }
}

