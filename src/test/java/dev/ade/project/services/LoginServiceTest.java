package dev.ade.project.services;

import dev.ade.project.daos.LoginDaoImpl;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import dev.ade.project.orm.AdeOrm;
import dev.ade.project.util.ConnectionUtil;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginServiceTest {

    @Spy
    AdeOrm mockOrm;

    @InjectMocks
    LoginService loginService;

    @BeforeAll
    public void setupMocks(){
        initMocks(this);
        loginService = new LoginService(new LoginDaoImpl(mockOrm));
    }

    @Test
    public void userCheckTest() throws ArgumentFormatException, SQLException {

        User user = new User("The","Tester",'M',"thetester","password");
        User spyUser = spy(user);

        when(mockOrm.get(any(String.class),any(Object.class))).thenReturn(spyUser);
        System.out.println(spyUser.getUsername());
        assertTrue(loginService.userCheck("thetester"));
    }

    @Test
    public void passCheckTest() throws ArgumentFormatException, SQLException {
        String username = "alpha";
        String pass = "password123";
        List<List<Object>> list = Arrays.asList(Arrays.asList(username,pass));

        when(mockOrm.getRecordsInOrder(any(List.class),any(String.class),any(Object.class),any(String.class),any(String.class)))
                .thenReturn(list);

        assertTrue(loginService.passCheck("alpha", "password123"));
    }

    @Test
    public void instantiateUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("The","Tester",'M',"thetester","password");
        User spyUser = spy(user);

        assertEquals(spyUser.toString(), loginService.instantiateUser("thetester").toString());
    }

}
