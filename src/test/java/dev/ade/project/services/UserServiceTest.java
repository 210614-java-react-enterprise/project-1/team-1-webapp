package dev.ade.project.services;

import dev.ade.project.daos.UserDao;
import dev.ade.project.exception.ArgumentFormatException;
import dev.ade.project.models.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest {
    @InjectMocks
    @Spy
    private UserService userService;

    @Mock
    UserDao userDao;

    @BeforeAll
    public void setupMocks(){
        initMocks(this);
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User user2 = new User("Richelle", "Hunt", 'F', "beta", "123");
        User user3 = new User("Jorge", "Olivero", 'M', "Charlie", "123");
        List<User> spyUsers = Arrays.asList(user, user2, user3);

        doReturn(spyUsers).when(userDao).getAll();
        doReturn(true).when(userDao).addUser(any(User.class));
        doReturn(true).when(userDao).updateUser(any(User.class));
        doReturn(true).when(userDao).deleteUser(any(Object.class));
        doReturn(user).when(userDao).getUser(any(String.class), any(String.class));

        userService = new UserService(userDao);
    }

    @Test
    public void getUserTest() throws ArgumentFormatException, SQLException {
        assertEquals("alpha", userService.getUser("alpha").getUsername());
    }

    @Test
    public void getAllTest() throws ArgumentFormatException, SQLException {
        assertEquals(3, userService.getAll().size());
    }

    @Test
    public void addUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userService.addUser(spyUser));
    }

    @Test
    public void deleteUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userService.deleteUser(spyUser.getUsername()));
    }

    @Test
    public void updateUserTest() throws ArgumentFormatException, SQLException {
        User user = new User("Leah", "Canavan", 'F', "alpha", "123");
        User spyUser = spy(user);
        assertTrue(userService.updateUser(spyUser));
    }
}
